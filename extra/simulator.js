$(document).ready( function() {
    $('.block-element').each( function() { handleOnMouseMoveGrid(this); });
        $(window).keypress(keypressHandler);
});
function keypressHandler(e){
    if (e.which == 32) {
        $('.scheme-link').each(function() {
            $(this).remove();
        });
        $('.input').each(function() {
            $(this).attr('visibility', '');
        });
        $('.scheme-element').each(function() {
            $(this).attr('input-links', '');
            $(this).attr('output-links', '');
        });
        $('.two-io > .output, .two-io > .input').each(function() {
            $(this).removeClass('input').removeClass('output').addClass('io');
            $(this).off('mousedown').mousedown(mousedownCircleOutputElement);
        });
        e.stopPropagation();
    }
}
class Generator {
    constructor() {
        this.inc = 0;
    }
let linkGenerator = new Generator;
let itemGenerator = new Generator;
function clone(cloned) {
    return cloned.cloneNode(true);
}
function handleOnMouseMoveGrid(element) {
    var selectedElement = element;
    var gridElement = $('#scheme-wrapper')[0];
    var draggableElement;
    selectedElement.onmousedown = mousedownBlockElement;
    selectedElement.parentNode.onmousedown = mousedownBlockElement;
    function mousedownBlockElement(e) {
        e = e || window.event;
        e.preventDefault();
        gridElement.onmouseenter = gridMouseEnter;
    }
    function gridMouseEnter(e) {
        draggableElement = clone(selectedElement);
        draggableElement.style.position = "absolute";
        draggableElement.onmousemove = handleGridMousemove;
        draggableElement.id = "draggable";
        $(draggableElement).find('*').attr('hidden', null);
        $(draggableElement).find('svg').attr('style', "margin-top:-4px; margin-left:-4px");
        $('body').append(draggableElement);
        draggableElement.onmouseup = mouseupDragElement;
        gridElement.onmouseenter = null;
        gridElement.onmousemove = elementDrag;
    }
	function handleGridMousemove(e) {
		gridElement.onmousemove(e)
	}
    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        if (draggableElement) {
            var gridOffsetX = e.clientX - gridElement.offsetLeft - 45;
			gridOffsetX = getInsideRoundCoord(gridOffsetX, 0, gridElement.clientWidth - draggableElement.clientWidth);
            let newX = gridElement.offsetLeft + gridOffsetX;
            var gridOffsetY = e.clientY - gridElement.offsetTop - 30;
			gridOffsetY = getInsideRoundCoord(gridOffsetY, 0, gridElement.clientHeight - draggableElement.clientHeight);
            let newY = gridElement.offsetTop + gridOffsetY;
            draggableElement.style.top = (newY) + "px";
            draggableElement.style.left = (newX) + "px";
        }
    }
    function mouseupDragElement() {
        var item = $('#draggable > svg > g');
        item.appendTo('#scheme-wrapper > svg')
        item.addClass('scheme-element');
		item.attr('id', 'item' + itemGenerator.generate);
        item.attr('input-links', '');
        item.attr('output-links', '');
        var x = draggableElement.offsetLeft - gridElement.offsetLeft;
        var y = draggableElement.offsetTop - gridElement.offsetTop;
		setItemCoords(item, x, y);
        item.mousedown(mousedownSchemeElement);
        item.click(clickSchemeElement);
        $(item).find('circle.io').mouseenter(mouseenterCircleIoElement)
        $(item).find('circle.io').mouseleave(mouseleaveCircleIoElement)
        $(item).find('circle.io').mousedown(mousedownCircleOutputElement)
        $(item).find('circle.input').mouseenter(mouseenterCircleIoElement)
        $(item).find('circle.input').mouseleave(mouseleaveCircleIoElement)
        $(item).find('circle.input').mousedown(mousedownCircleInputElement)
        $(item).find('circle.output').mouseenter(mouseenterCircleIoElement)
        $(item).find('circle.output').mouseleave(mouseleaveCircleIoElement)
        $(item).find('circle.output').mousedown(mousedownCircleOutputElement)
        $(item).find('path.switchable-input').click(clickSwitchableInput)
        gridElement.onmousemove = null;
        $(draggableElement).remove();
        draggableElement = null;
    }
}
function clickSwitchableInput(e) {
    let link = $(this.parentNode).find('#' + this.id + '.input').attr('link');
    if ($(this).hasClass('plus-input')) {
        $(this).removeClass('plus-input');
        $(this).addClass('minus-input');
        if (link) {
            $('#' + link).attr('positive', false);
        }
    } else {
        $(this).removeClass('minus-input');
        $(this).addClass('plus-input');
        if (link) {
            $('#' + link).attr('positive', true);
        }
    }
    e.stopPropagation()
}
function mouseenterCircleIoElement(e) {
    $(this).attr('r', '6');
}
function mouseleaveCircleIoElement(e) {
    $(this).attr('r', '4');
}
function mouseenterWarningCircleOutputElement(e) {
    $(this).addClass('warning');
}
function mouseleaveCircleOutputElement(e) {
    $(this).removeClass('warning');
}
function mousedownCircleInputElement(e) {
    e.stopPropagation();
}
const SVG_NS = "http://www.w3.org/2000/svg";
function mousedownCircleOutputElement(e){
    var gridElement = $('#scheme-wrapper')[0];
	var linkElement = createLinkElement();
	$(linkElement).attr('start-element', this.parentNode.id);
	$(linkElement).attr('start-io', this.id);
	var x1, x2, y1, y2;
	initCoordinates();
    $('#scheme').prepend(linkElement);
	setEvents();
    e.stopPropagation();
	function createLinkElement() {
		var result = document.createElementNS(SVG_NS, 'polyline');
		$(result).attr('class', 'scheme-link');
		$(result).attr('marker-end', 'url(#tmp-arrow-marker)');
		$(result).attr('start-element', '');
		$(result).attr('start-io', '');
		$(result).attr('end-element', '');
        $(result).attr('end-io', '');
        $(result).attr('positive', '');
		return result;
	}
	function initCoordinates() {
		x1 = getX(e);
		y1 = getY(e);
		x2 = x1;
		y2 = y1;
		setArrowCoordinates();
	}
    function getX(e) {
        var x = e.clientX - gridElement.offsetLeft;
        return getRoundCoord(x);
    }
    function getY(e) {
        var y = e.clientY - gridElement.offsetTop;
        return getRoundCoord(y);
    }
    function mousemoveGridElement(e) {
		x2 = getX(e);
		y2 = getY(e);
		setArrowCoordinates();
    }
    function mouseupGridElement(e) {
        removeLink();
        stopEvents();
    }
    function mouseupCircleIoElement(e) {
        removeZeroLink();
        if (linkElement) {
            var id = 'link' + linkGenerator.generate;
            $(linkElement).attr('id', id);
            $(linkElement).attr('end-io', this.id);
			$(linkElement).attr('end-element', this.parentNode.id);
			let markerDirection = $(this).attr('marker-direction');
			if (markerDirection) {
                $(linkElement).attr('marker-end', 'url(#arrow-marker-' + markerDirection + ')');
			}
            $(this).attr('visibility', 'hidden');
			let startElementId = $(linkElement).attr('start-element');
			let startIoId = $(linkElement).attr('start-io');
			var startIo = $('#' + startElementId + ' > #' + startIoId);
            startIo.attr('class', 'output');
            if ( $('#' + startElementId).hasClass('two-io') ) {
                var input = $('#' + startElementId + ' > .io')
                input.removeClass('io').addClass('input');
                input.unbind('mousedown', mousedownCircleOutputElement).mousedown(mousedownCircleInputElement)
            }
            $(this).removeClass('io').addClass('input');
            if ( $(this.parentNode).hasClass('two-io') ) {
                var output = $(this.parentNode).find('.io')
                output.removeClass('io').addClass('output');
            }
            $('#' + startElementId).attr('output-links', function(i, val) {
                return val + " " + id;
            });
            $(this.parentNode).attr('input-links', function(i, val) {
                return val + " " + id;
            });
            if ($(this.parentNode).attr('type') == 'summator' && $(this.parentNode).find('#' + this.id + '.switchable-input').hasClass('minus-input')) {
                $(linkElement).attr('positive', false);
            } else {
                $(linkElement).attr('positive', true);
            }
            $(this).attr('link', $(linkElement).attr('id'));
        }
        stopEvents();
        e.stopPropagation();
    }
	function setEvents() {
		$(gridElement).mousemove(mousemoveGridElement);
		$(gridElement).mouseup(mouseupGridElement);
		$(gridElement).find('circle.io').mouseup(mouseupCircleIoElement);
		$(gridElement).find('circle.input').mouseup(mouseupCircleIoElement);
		$(gridElement).find('circle.output').mouseenter(mouseenterWarningCircleOutputElement);
		$(gridElement).find('circle.output').mouseleave(mouseleaveCircleOutputElement);
	}
    function stopEvents() {
        $(gridElement).off('mousemove');
        $(gridElement).off('mouseup');
        $(gridElement).find('circle.io').off('mouseup');
		$(gridElement).find('circle.input').off('mouseup');
		$(gridElement).find('circle.output').off('mouseup');
		$(gridElement).find('circle.output').unbind('mouseenter', mouseenterWarningCircleOutputElement);
    }
    function removeLink() {
	    $(linkElement).remove();
	    linkElement = null;
    }
    function removeZeroLink() {
        if ( (x1 == x2) && (y1 == y2) ) {
            removeLink();
        }
    }
    function setArrowCoordinates() {
		setLinkCoords($(linkElement), x1, y1, x2, y2);
    }
}
function mousedownSchemeElement(e) {
    var selectedElement = this;
    var gridElement = $('#scheme-wrapper')[0];
    var originX = $(selectedElement).attr('x');
    var originY = $(selectedElement).attr('y');
    gridElement.onmousemove = moveElement;
	gridElement.onmouseup = stopMoveElement;
	function moveElement(e) {
		let gridOffsetX = e.clientX - gridElement.offsetLeft - 45;
		let newX = getInsideRoundCoord(gridOffsetX, 0, gridElement.clientWidth - selectedElement.clientWidth);
		let gridOffsetY = e.clientY - gridElement.offsetTop - 30;
		let newY = getInsideRoundCoord(gridOffsetY, 0, gridElement.clientHeight - selectedElement.clientHeight);
		setItemCoords($(selectedElement), newX, newY);
		let dx = newX - originX;
		let dy = newY - originY;
		moveInputLinks(dx, dy);
		moveOutputLinks(dx, dy);
		originX = newX;
		originY = newY;
	}
	function stopMoveElement() {
		gridElement.onmousemove = null;
		gridElement.onmouseup = null;
	}
	function moveInputLinks(dx, dy) {
		moveLinks('input-links', dx, dy, function(link, dx, dy) {
			let coords = getLinkCoords(link);
	        let x2 = Number(coords[2]) + dx;
	        let y2 = Number(coords[3]) + dy;
			setLinkCoords(link, coords[0], coords[1], x2, y2);
		});
	}
	function moveOutputLinks(dx, dy) {
		moveLinks('output-links', dx, dy, function(link, dx, dy) {
			let coords = getLinkCoords(link);
			let x1 = Number(coords[0]) + dx;
			let y1 = Number(coords[1]) + dy;
			setLinkCoords(link, x1, y1, coords[2], coords[3]);
		});
	}
	function moveLinks(linkType, dx, dy, handler) {
	    var links = $(selectedElement).attr(linkType).split(' ');
	    links.forEach(function (item, i, arr) {
	        if (item) {
	            var link = $(gridElement).find('#' + item);
				handler(link, dx, dy);
	        }
	    });
	}
}
function clickSchemeElement(e) {
    var selectedItem = this;
    var selectedItemId = this.id;
    $('#params-pane').removeClass('d-none');
    $('#params-pane > #item-id').html(selectedItemId);
    var params = $(selectedItem).attr('params');
    var inputGroups = $('#item-params').find('.input-group');
    var i = 0;
    if (params) {
        params.split(',').forEach(function(param) {
            var paramValue = $(selectedItem).attr(param.toLowerCase());
            var input;
            if (i < inputGroups.length) {
                var inputGroup = inputGroups.slice(i, i + 1);
                inputGroup.find('.input-group-text').html(param);
                input = inputGroup.find('input');
                input[0].value = paramValue;
                input.off('input')
            } else {
                input = $('<input class="form-control" type="text" value="' + paramValue + '"/>');
                inputGroup = $('<div class="input-group mb-3"></div>');
                inputGroup.append('<div class="input-group-prepend"><span class="input-group-text">' + param + '</span></div>')
                          .append(input);
                $("#item-params").append(inputGroup);
            }
            input.attr('param', param);
            input.on('input', function(){
                $(selectedItem).attr($(this).attr('param').toLowerCase(), this.value);
            });
            i++;
        });
    }
    inputGroups.slice(i).remove();
    $('#remove-item-btn').off('click').click(removeSelectedElement);
    function removeSelectedElement() {
        removeSchemeElement(selectedItem);
        $('#params-pane').addClass('d-none');
    }
}
function removeSchemeElement(schemeElement) {
    $(schemeElement).attr('input-links').split(' ').forEach(removeInputLink);
    $(schemeElement).attr('output-links').split(' ').forEach(removeOutputLink);
    $(schemeElement).remove();
}
function removeInputLink(linkId, i, arr) {
    if (linkId) {
        var link = $('#' + linkId);
        var startElementId = link.attr('start-element');
        link.remove();
        processRemoveLink(startElementId, linkId, 'output');
    }
}
function removeOutputLink(linkId, i, arr) {
    if (linkId) {
        var link = $('#' + linkId);
        var endElementId = link.attr('end-element');
        var endIoId = link.attr('end-io');
        var endIo = $('#' + endElementId + ' > #' + endIoId);
        endIo.attr('visibility', null);
        link.remove();
        processRemoveLink(endElementId, linkId, 'input');
    }
}
function processRemoveLink(schemeElementId, linkId, linkType) {
    var schemeElement = $('#' + schemeElementId);
    if (linkType == 'input') {
        var inputLinks = schemeElement.attr('input-links');
        inputLinks = inputLinks.replace(linkId, '');
        schemeElement.attr('input-links', inputLinks);
    } else {
        var outputLinks = schemeElement.attr('output-links');
        outputLinks = outputLinks.replace(linkId, '');
        schemeElement.attr('output-links', outputLinks);
    }
    if (schemeElement.hasClass('two-io')) {
        processDoubleIoRemoveLink(schemeElement);
    }
}
function processDoubleIoRemoveLink(schemeElement) {
    var inputLinks = schemeElement.attr('input-links');
    var outputLinks = schemeElement.attr('output-links');
    inputLinks = inputLinks.replace(' ', '');
    outputLinks = outputLinks.replace(' ', '');
    if (!inputLinks && !outputLinks) {
        schemeElement.find('.output, .input').each(function() {
            $(this).removeClass('input').removeClass('output').addClass('io');
            $(this).off('mousedown').mousedown(mousedownCircleOutputElement);
        });
    }
}
function getLinkCoords(link) {
	var x1 = link.attr('x1');
	var y1 = link.attr('y1');
	var x2 = link.attr('x2');
	var y2 = link.attr('y2');
	return [x1, y1, x2, y2];
}
function setItemCoords(item, x, y) {
	item.attr('transform', 'translate(' + x + ',' + y + ')');
	item.attr('x', x);
	item.attr('y', y);
}
function setLinkCoords(link, x1, y1, x2, y2) {
    let p1 = x1 + ',' + y1;
    let p3 = x2 + ',' + y2;
    var p2;
    if ( Math.abs(x2 - x1) >= Math.abs(y2 - y1) ) {
        p2 = x2 + ',' + y1;
    } else {
        p2 = x1 + ',' + y2;
    }
    if (p2 == p3){
	    link.attr("points", p1 + ' ' + p2 );
    } else {
        link.attr("points", p1 + ' ' + p2 + ' ' + p3);
    }
	link.attr('x1', x1);
	link.attr('y1', y1);
	link.attr('x2', x2);
	link.attr('y2', y2);
}
function getInsideCoord(x, x1, x2) {
	if (x < x1) {
		x = x1;
	} else if (x > x2) {
		x = x2;
	}
	return x;
}
function getRoundCoord(x) {
	return Math.round(x/15) * 15;
}
function getInsideRoundCoord(coord, coord1, coord2) {
	return getRoundCoord(getInsideCoord(coord, coord1, coord2));
}
function makeTransferFunction(element) {
    var tf = element.attr('transfer-function');
    var params = element.attr('params');
    if (params) {
        params.split(',').forEach(function(param) {
            var paramValue = element.attr(param.toLowerCase());
            var regexp = new RegExp(param, 'g');
            tf = tf.replace(regexp, paramValue);
        });
    }
    return eval('`' + tf + '`');
}
function makeInputFunction(element) {
    var tf = element.attr('input-function');
    var params = element.attr('params');
    if (params) {
        params.split(',').forEach(function(param) {
            var paramValue = element.attr(param.toLowerCase());
            var regexp = new RegExp(param, 'g');
            tf = tf.replace(regexp, paramValue);
        });
    }
    return eval('`' + tf + '`');
}
function makeScheme() {
    var scheme = { 'items': [], 'links': [] };
    $('.scheme-element').each(function(index,value) {
        var item = {};
        item.id = value.id;
        item.type = $(value).attr('type');
        if (item.type == 'element') {
            item.tf = makeTransferFunction($(value));
        } else if (item.type == 'input-signal') {
            item.input = makeInputFunction($(value));
            item.inputType = $(value).attr('input-type');
        }
        scheme.items.push(item);
    });
    $('.scheme-link').each(function(index,value) {
        var link = {};
        link.id = value.id;
        link.start = $(value).attr('start-element');
        link.end = $(value).attr('end-element');
        link.positive = $(value).attr('positive');
        scheme.links.push(link);
    });
    $('#sim-time').each(function() {
        scheme.time = this.value;
    })
    return scheme;
}
function stepResponse() {
    var scheme = makeScheme();
    $.ajax({
        type: "POST",
        url: "step-response",
        data: JSON.stringify(scheme),
        success: onAjaxResponse,
        error: onAjaxError,
        contentType: 'application/json'
    });
}
function impulseResponse() {
    var scheme = makeScheme();
    $.ajax({
        type: "POST",
        url: "impulse-response",
        data: JSON.stringify(scheme),
        success: onAjaxResponse,
        error: onAjaxError,
        contentType: 'application/json'
    });
}
function forcedResponse() {
    var scheme = makeScheme();
    $.ajax({
        type: "POST",
        url: "forced-response",
        data: JSON.stringify(scheme),
        success: onAjaxResponse,
        error: onAjaxError,
        contentType: 'application/json'
    });
}
function bodePlot() {
    var scheme = makeScheme();
    $.ajax({
        type: "POST",
        url: "bode-plot",
        data: JSON.stringify(scheme),
        success: onAjaxResponse,
        error: onAjaxError,
        contentType: 'application/json'
    });
}
function nyquistPlot() {
    var scheme = makeScheme();
    $.ajax({
        type: "POST",
        url: "nyquist-plot",
        data: JSON.stringify(scheme),
        success: onAjaxResponse,
        error: onAjaxError,
        contentType: 'application/json'
    });
}
function onAjaxResponse(data) {
    var downloadingImage = new Image();
    downloadingImage.onload = function(){
        $('#result-image').attr('src', this.src)
        $('#result-background').attr('hidden', null);
        $('#image-wrapper').attr('hidden', null);
    };
    downloadingImage.src = 'img/' + data;
}
function onAjaxError(xhr, status, error) {
    $('#result-error').html(xhr.responseText);
    $('#result-background').attr('hidden', null);
    $('#error-wrapper').attr('hidden', null);
}
function hideImage() {
    $('#image-wrapper').attr('hidden', '');
    $('#error-wrapper').attr('hidden', '');
    $('#result-background').attr('hidden', '');
}
